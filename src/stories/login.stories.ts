import { storiesOf, moduleMetadata } from "@storybook/angular";
import { StoreModule } from "@ngrx/store";
import { VerifyMfaComponent } from "../app/login/verify-mfa/verify-mfa.component";
import * as fromVerifyMfa from "../app/login/verify-mfa/verify-mfa.reducer";
import { SharedModule } from "../app/shared/shared.module";
import { HttpClientModule } from "@angular/common/http";
import { InlineSVGModule } from "ng-inline-svg";
import { LoginComponent } from "../app/login/login.component";
import * as fromLogin from "../app/login/login.reducer";
import { NgrxFormsModule } from "ngrx-forms";
import { RouterTestingModule } from "@angular/router/testing";
import { ProgressIndicatorModule } from "../app/progress-indicator/progress-indicator.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LoginWrapperComponent } from "../app/login/login-wrapper/login-wrapper.component";

storiesOf("Login", module)
  .addDecorator(
    moduleMetadata({
      imports: [
        SharedModule,
        StoreModule.forRoot({}),
        HttpClientModule,
        InlineSVGModule.forRoot(),
        HttpClientModule,
        NgrxFormsModule,
        SharedModule,
        RouterTestingModule,
        ProgressIndicatorModule,
        BrowserAnimationsModule,
        StoreModule.forRoot({})
      ],
      declarations: [LoginWrapperComponent]
    })
  )
  .add("Login Wrapper", () => ({
    component: LoginWrapperComponent,
    props: {}
  }))
  .add("Login", () => ({
    component: LoginComponent,
    props: {
      form: fromLogin.initialLoginFormState.form
    }
  }))

  .add("Verify MFA", () => {
    return {
      component: VerifyMfaComponent,
      props: {
        form: fromVerifyMfa.initialState.form
      }
    };
  });
