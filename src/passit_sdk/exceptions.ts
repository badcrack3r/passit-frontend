/* tslint:disable:max-classes-per-file */
/**
 * Class for throwing exceptions with a response
 */
export class ResponseException extends Error {

    /**
     * Init
     * @param res - the response json or text
     * @param message - the error message
     */
    constructor(public res: {}, message: string) {
        super(message);
        this.name = "Passit SDK Response Error";
    }
}

/** An SDK error that isn't from a server response.
 * For example failed crypto.
 */
export class SDKException extends Error {
    /**
     * Init
     * @param err - the caught error
     * @param message - the error message
     */
    constructor(public err: any, message: string) {
        super(message);
        this.err = err;
        this.name = "Passit SDK Error";
    }
}

/**
 * Raised when a method is used before authentication
 */
export class AuthenticationRequiredException extends Error {
    /**
     * @param message - the error message
     */
    constructor(message: string) {
        super(message + " requires authentication");
        this.name = "Passit SDK Authentication Error";
    }
}
