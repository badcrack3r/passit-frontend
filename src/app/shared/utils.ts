import { HttpResponse } from "@angular/common/http";

/** Generic handler to turn http responses into human readable messages */
export const checkRespForErrors = <T>(res: HttpResponse<T>) => {
  const errors: string[] = [];
  if (res.status === 0) {
    errors.push("No Internet connection. Try again when you're back online.");
  } else if (res.status === 500) {
    errors.push("Server error. 500 error.");
  } else if (res.status === 504) {
    errors.push("Unable to contact server - 504 error.");
  }
  return errors;
};
