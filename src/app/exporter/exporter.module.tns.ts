import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { ExporterComponent } from "./exporter.component";
import { ExporterService } from "./exporter.service";
import { ExporterRoutingModule } from "./exporter-routing.module";
import { SharedModule } from "../shared/shared.module";
import { MobileMenuModule } from "../mobile-menu";

@NgModule({
  declarations: [ExporterComponent],
  imports: [
    NativeScriptCommonModule,
    ExporterRoutingModule,
    SharedModule,
    MobileMenuModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [ExporterService]
})
export class ExporterModule {}
