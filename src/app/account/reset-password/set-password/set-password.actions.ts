import { createAction } from "@ngrx/store";

export const setPassword = createAction("[Set Password] Set Password");
export const setPasswordSuccess = createAction(
  "[Set Password] Set Password Success"
);
export const setPasswordFailure = createAction(
  "[Set Password] Set Password Failure"
);
export const forceSetPassword = createAction(
  "[Set Password] Force Set Password"
);
