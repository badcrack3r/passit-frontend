import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { of } from "rxjs";
import { mergeMap, withLatestFrom, map, catchError } from "rxjs/operators";
import { IState, selectAllGroups, getSecrets } from "../../../app.reducers";
import { UserService } from "../../user";
import * as SetPasswordActions from "./set-password.actions";
import {
  getSetPasswordForm,
  getResetPasswordVerifyEmailAndCode,
  getSetPasswordBackupCode
} from "~/app/account/account.reducer";

@Injectable()
export class SetPasswordEffects {
  setPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SetPasswordActions.setPassword),
      withLatestFrom(
        this.store.select(getSetPasswordForm),
        this.store.select(getResetPasswordVerifyEmailAndCode),
        this.store.select(getSetPasswordBackupCode),
        this.store.select(getSecrets),
        this.store.select(selectAllGroups)
      ),
      mergeMap(([action, form, emailCode, backupCode, secrets, groups]) =>
        this.userService
          .resetPasswordSetPassword(
            emailCode.emailCode!,
            form.value.newPassword,
            backupCode!,
            secrets,
            groups
          )
          .pipe(
            map(() => SetPasswordActions.setPasswordSuccess()),
            catchError(() => of(SetPasswordActions.setPasswordFailure()))
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private userService: UserService
  ) {}
}
