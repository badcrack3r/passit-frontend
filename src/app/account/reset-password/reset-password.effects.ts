import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { of, concat, timer } from "rxjs";
import {
  StartAsyncValidationAction,
  ClearAsyncErrorAction,
  SetAsyncErrorAction
} from "ngrx-forms";
import {
  withLatestFrom,
  map,
  exhaustMap,
  catchError,
  filter,
  distinctUntilChanged,
  switchMap
} from "rxjs/operators";
import {
  SubmitForm,
  ResetPasswordActionTypes,
  SubmitFormSuccess,
  SubmitFormFailure
} from "./reset-password.actions";
import { IState } from "../../app.reducers";
import { getResetPasswordForm } from "../account.reducer";
import { UserService } from "../user";

@Injectable()
export class ResetPasswordEffects {
  @Effect()
  submitForm$ = this.actions$.pipe(
    ofType<SubmitForm>(ResetPasswordActionTypes.SUBMIT_FORM),
    withLatestFrom(this.store.pipe(select(getResetPasswordForm))),
    map(([action, form]) => form.value),
    exhaustMap(form => {
      const callForgotPassword = () => {
        return this.userService.resetPassword(form.email).pipe(
          map(() => new SubmitFormSuccess()),
          catchError(err => of(new SubmitFormFailure()))
        );
      };
      const callCheckAndSetUrl = (url: string) => {
        return this.userService.checkAndSetUrl(url).pipe(
          exhaustMap(() => callForgotPassword()),
          catchError(err => of(new SubmitFormFailure()))
        );
      };

      if (form.url) {
        return callCheckAndSetUrl(form.url);
      } else {
        return callForgotPassword();
      }
    })
  );

  @Effect()
  asyncServerUrlCheck$ = this.store.pipe(select(getResetPasswordForm)).pipe(
    filter(form => form.value.showUrl),
    distinctUntilChanged(
      (first, second) => first.value.url === second.value.url
    ),
    switchMap(form =>
      concat(
        timer(300).pipe(
          map(
            () => new StartAsyncValidationAction(form.controls.url.id, "exists")
          )
        ),
        this.userService.checkUrl(form.value.url).pipe(
          map(() => new ClearAsyncErrorAction(form.controls.url.id, "exists")),
          catchError(() => [
            new SetAsyncErrorAction(
              form.controls.url.id,
              "exists",
              form.value.url
            )
          ])
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private userService: UserService
  ) {}
}
