import { Component, OnInit } from "@angular/core";
import * as fromAccount from "../../account.reducer";
import { Store, select } from "@ngrx/store";
import { ActivatedRoute } from "@angular/router";
import { init, verifyAndLogin } from "./reset-password-verify.actions";
import { BACKUP_CODE_CHARS, BACKUP_CODE_LENGTH } from "../constants";
import { IState } from "../../../app.reducers";

@Component({
  template: `
    <app-reset-password-verify
      [form]="form$ | async"
      [hasStarted]="hasStarted$ | async"
      [errorMessage]="errorMessage$ | async"
      [badCodeError]="badCodeError"
      (verify)="verify($event)"
    ></app-reset-password-verify>
  `
})
export class ResetPasswordVerifyContainer implements OnInit {
  form$ = this.store.pipe(select(fromAccount.getResetPasswordVerifyForm));
  hasStarted$ = this.store.pipe(
    select(fromAccount.getResetPasswordVerifyHasStarted)
  );
  errorMessage$ = this.store.pipe(
    select(fromAccount.getResetPasswordVerifyErrorMessage)
  );
  badCodeError: string;

  constructor(private store: Store<IState>, private route: ActivatedRoute) {}

  ngOnInit() {
    const payload = {
      code: this.route.snapshot.queryParamMap.get("code"),
      email: this.route.snapshot.queryParamMap.get("email")
    };
    this.store.dispatch(init(payload));
  }

  verify(backupCode: string) {
    backupCode = backupCode.replace(/\s/g, "").toUpperCase();
    if (
      backupCode.length === BACKUP_CODE_LENGTH &&
      !backupCode.match(new RegExp(`[^${BACKUP_CODE_CHARS}\-]`))
    ) {
      this.store.dispatch(verifyAndLogin({ payload: backupCode }));
    } else {
      // This error is for coedes that don't match the 32 letter and spacing requirements.
      // If it is a Passit code, but the wrong one, it will error in in effects.
      this.badCodeError = "Passit does not recognize this backup code.";
    }
  }
}
