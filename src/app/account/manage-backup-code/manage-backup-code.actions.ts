import { Action } from "@ngrx/store";

export enum ManageBackupCodeActionTypes {
  SUBMIT_FORM = "[Manage Backup Code] Submit",
  SUBMIT_FORM_SUCCESS = "[Manage Backup Code] Submit Success",
  SUBMIT_FORM_FAILURE = "[Manage Backup Code] Submit Failure",
  RESET_FORM = "[Manage Backup Code] Reset"
}

export class SubmitForm implements Action {
  readonly type = ManageBackupCodeActionTypes.SUBMIT_FORM;
}

export class SubmitFormSuccess implements Action {
  readonly type = ManageBackupCodeActionTypes.SUBMIT_FORM_SUCCESS;

  constructor(public payload: string) {}
}

export class SubmitFormFailure implements Action {
  readonly type = ManageBackupCodeActionTypes.SUBMIT_FORM_FAILURE;

  constructor(public payload: string[]) {}
}

export class ResetForm implements Action {
  readonly type = ManageBackupCodeActionTypes.RESET_FORM;
}

export type ManageBackupCodeActionsUnion =
  | SubmitForm
  | SubmitFormSuccess
  | SubmitFormFailure
  | ResetForm;
