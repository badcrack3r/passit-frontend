import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { UserService } from "../user";
import { Store, select } from "@ngrx/store";
import { IState } from "~/app/app.reducers";
import { withLatestFrom, exhaustMap, map, catchError } from "rxjs/operators";
import {
  DeleteAccountActionTypes,
  DeleteAccountFailure
} from "./delete.actions";
import * as fromAccount from "../account.reducer";
import { of } from "rxjs";
import { LogoutSuccessAction } from "../account.actions";

@Injectable()
export class DeleteEffects {
  @Effect()
  deleteAccount$ = this.actions$.pipe(
    ofType(DeleteAccountActionTypes.DELETE_ACCOUNT),
    withLatestFrom(this.store.pipe(select(fromAccount.getDeleteForm))),
    exhaustMap(([action, form]) =>
      this.userService.deleteUserAccount(form.value.password).pipe(
        map(() => new LogoutSuccessAction()),
        catchError(err => of(new DeleteAccountFailure(err)))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private store: Store<IState>
  ) {}
}
