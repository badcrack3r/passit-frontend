// import { IContact } from "../group/contacts/contacts.interfaces";

export const setContacts = (
  contactsArray: any[],
  isDisabled: boolean | false
) => {
  return contactsArray.map((contact: any) => {
    return {
      label: contact.email,
      value: contact.id,
      disabled: isDisabled
    };
  });
};
