import { props, createAction } from "@ngrx/store";
import { IGroup } from "../../passit_sdk/api.interfaces";

export const loadGroups = createAction("[Groups] Load Groups");
export const setGroups = createAction(
  "[Groups] Set Groups",
  props<{ groups: IGroup[] }>()
);
