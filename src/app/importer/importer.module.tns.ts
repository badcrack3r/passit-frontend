import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { StoreModule } from "@ngrx/store";
import { TNSCheckBoxModule } from "nativescript-checkbox/angular";

import { ImporterComponent } from "./importer.component";
import { ImporterService } from "./importer.service";
import { ImporterContainer } from "./importer.container";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";
import { importerReducer } from "./importer.reducer";
import { ImporterRoutingModule } from "./importer-routing.module";
import { SharedModule } from "../shared/shared.module";
import { MobileMenuModule } from "../mobile-menu";

@NgModule({
  declarations: [ImporterComponent, ImporterContainer],
  imports: [
    NativeScriptCommonModule,
    ProgressIndicatorModule,
    SharedModule,
    MobileMenuModule,
    TNSCheckBoxModule,
    ImporterRoutingModule,
    StoreModule.forFeature("importer", importerReducer)
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [ImporterService]
})
export class ImporterModule {}
