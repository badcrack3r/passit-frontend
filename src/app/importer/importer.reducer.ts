import { ImportableSecret } from "./importableSecret";
import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromRoot from "../app.reducers";
import { ImporterActionTypes, Actions } from "./importer.actions";

export interface IImporterState {
  fileName: string | null;
  secrets: ImportableSecret[] | null;
}

export const initialState: IImporterState = {
  fileName: null,
  secrets: null
};

export function importerReducer(
  state = initialState,
  action: Actions
): IImporterState {
  switch (action.type) {
    case ImporterActionTypes.SET_FILE_NAME:
      return Object.assign({}, state, {
        fileName: action.payload
      });

    case ImporterActionTypes.RESET_FILE_NAME:
      return Object.assign({}, state, {
        fileName: null
      });

    case ImporterActionTypes.RESET_IMPORT_SECRETS:
      return Object.assign({}, state, {
        secrets: null
      });

    case ImporterActionTypes.SET_IMPORT_SECRETS:
      return Object.assign({}, state, {
        secrets: action.payload
      });

    default:
      return state;
  }
}

export interface IState extends fromRoot.IState {
  importer: IImporterState;
}

export const getState = createFeatureSelector<IState, IImporterState>(
  "importer"
);
export const getFileName = createSelector(
  getState,
  (state: IImporterState) => state.fileName
);
export const getSecrets = createSelector(
  getState,
  (state: IImporterState) => state.secrets
);
